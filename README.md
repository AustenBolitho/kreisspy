# A library for calculating the transient amplification of ODEs around a linear fixed point
This library calculates the values of the Kreiss constant and duration of transient amplifications for an input matrix. Right now this is simplified into a single convenience function.

Depends on numpy, scipy, matplotlib and pyross. For pseudospectral plots, pseudopy can be used.

Example:

```python
    import numpy as np
    from kreisspy import characterize_transient, plot
    M = np.array([[-1,100],[0,-1]])
    print(characterize_transient(M))
```
    >>> [[array([-1.+0.j, -1.+0.j]), 49.00000000000001, 25.009999621075423, 3.217493793379348]]
    
This returns the most positive eigenvalues, the initial exponential growth rate, 
the Kreiss constant and a measure of the duration of the transient.

```python
    t=np.arange(0,7,70)
    plot(t, M, [1,1], plot_all=False, matrix_characteristic=True)
```
    
![alt text](./fig1.png)

## Installation
### Dependencies
Kreisspy depends on numpy, scipy and matplotlib. Please install these first. 
Then clone (or download) the repository and use a terminal to install using 

```bash
>> git clone https://gitlab.com/AustenBolitho/kreisspy.git
>> cd kreisspy
>> python setup.py install
```

## License
KreissPy is free software licensed under the [MIT License](http://opensource.org/licenses/mit-license.php).