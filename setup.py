# -*- coding: utf-8 -*-
import os
from distutils.core import setup
import codecs

# shamelessly copied from pseudopy which was shamelessly copied from VoroPy
def read(fname):
    return codecs.open(os.path.join(os.path.dirname(__file__), fname), encoding='utf-8').read()

setup(name='kreisspy',
      packages=['kreisspy'],
      version='0.1.0',
      description='Characterising transient behaviour of linear systems',
      long_description=read('README.md'),
      author='Austen Bolitho',
      author_email='ab2075@cam.ac.uk',
      url='https://gitlab.com/AustenBolitho/kreisspy',
      install_requires=['matplotlib>=3.13', 'numpy>=1.18.1',
                'scipy>=1.4.1'],
      classifiers=[
          'Development Status :: 1 - alpha',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: MIT License',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3',
          'Topic :: Scientific/Engineering :: Mathematics'
          ],
      )