# -*- coding: utf-8 -*-
# appropriated from pseudopy

import numpy
from scipy.linalg import toeplitz
from scipy.sparse import csr_matrix
from kreisspy import plot, characterize_transient


def toeplitz1(N):
    column = numpy.zeros(N)
    column[1] = 0.25
    row = numpy.zeros(N)
    row[1] = 1
    return csr_matrix(toeplitz(column, row))


def grcar(N, k=3):
    column = numpy.zeros(N)
    column[0:2] = [1, -1]
    row = numpy.zeros(N)
    row[0:k+1] = 1
    return csr_matrix(toeplitz(column, row))


def grcar_demo():

    # get Grcar matrix
    A = grcar(32).todense()

    # compute pseudospectrum
    char = characterize_transient(A)
    
    # plot
    plot(numpy.arange(0,5,.01), A, numpy.arange(0,32), 
         matrix_characteristic=char)
    

if __name__ == '__main__':
    grcar_demo()