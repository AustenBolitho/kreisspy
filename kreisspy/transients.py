import numpy as np
import scipy.linalg as spl
import scipy.integrate as spi
import warnings, doctest


def _epsilon_eval(z, A, ord=2):
    """
    Finds the value of \epsilon for a given complex number and matrix.
    Uses the first definition of the pseudospectrum in Trfethen & Embree
    ord="svd" uses fourth definition (may be faster)
    

    inputs:
    z: length 2 array representing a complex number
    A: an MxM matrix
    order: order of the matrix norm given from associated vector norm
    default is regular L2 norm -> returns maximum singular value.
    accepted inputs are any in spl.norm or "svd"  
    """
    z=np.array(z)
    A=np.array(A)
    zc = complex(z[0], z[1])
    try :
        ep = 1/spl.norm(spl.inv(zc*np.eye(*A.shape)-A),ord=ord)
    except TypeError:
        if ord=="svd":
            ep = np.min(spl.svdvals(zc*np.eye(*A.shape)-A))
        else: raise Exception("invalid method")
    return ep


def _kreiss_eval(z, A, theta=0, ord=2):
    """
    Kreiss constant guess for a matrix and pseudo-eigenvalue.

    inputs:
    z: length 2 array representing a complex number
    A: an MxM matrix
    theta: normalizing factor found in Townley et al 2007, default 0
    ord: default 2, order of matrix norm
    """
    z=np.array(z)
    A=np.array(A)
    kg = (z[0]-theta)/_epsilon_eval(z, A, ord=ord)
    return kg


def _inv_kreiss_eval(z, A, theta=0, ord=2):
    """
    1/Kreiss constant guess for a matrix and pseudo-eigenvalue.
    for minimizer

    inputs:
    z: length 2 array representing a complex number
    A: an MxM matrix
    theta: normalizing factor found in Townley et al 2007, default 0
    ord: default 2, order of matrix norm
    """
    return 1/_kreiss_eval(z, A, theta, ord)


def _kreiss_eval_old(z, A):
    """
    Kreiss constant for a matrix and pseudo-eigenvalue guess

    inputs:
    z: length 2 array representing a complex number
    A: an MxM matrix
    """
    warnings.warn("this function will be depricated", DeprecationWarning)
    z=np.array(z)
    A=np.array(A)
    zc = complex(z[0], z[1])
    kg = np.min(spl.svdvals((zc*np.eye(*A.shape)-A)/zc.real)) if zc.real > 0 else np.inf
    return kg
    

def _transient_properties( guess, A, theta=0, ord=2):
    """
    returns the maximal eigenvalue (spectral abcissa),
    initial groth rate (numerical abcissa),
    the Kreiss constant (minimum bound of transient)
    and time of transient growth

    inputs:
    A: an MxM matrix
    guess: initial guess for the minimizer
    theta: normalizing factor found in Townley et al 2007, default 0
    ord: default 2, order of matrix norm

    returns: [Kreiss constant, numerical abcissa, duration of transient,
              henrici's departure from normalcy']
    """
    from scipy.optimize import minimize
    A = np.array(A)
    if np.array_equal(A@A.T, A.T@A):
        warnings.warn("The input matrix is normal")
        # print("The input matrix is normal")
    evals = spl.eigvals(A)
    sa = evals[
        np.where(np.real(evals) == np.amax(np.real(evals)))[0]
    ]
    na = np.real(np.max(spl.eigvals((A+A.T)/2)))
    m = minimize(_inv_kreiss_eval, guess, args=(A, theta, ord),
                 bounds=((0, None), (None, None)))
    K = 1/m.fun
    tau = np.log(1/m.fun)/m.x[0]
    evals2 = np.dot(evals,np.conj(evals))
    frobNorm = spl.norm(A,ord='fro')
    henrici = np.sqrt(frobNorm**2-evals2)#/frobNorm
    return np.array([sa, na, K, tau, henrici],dtype=np.complex64)


def _first_estimate(A, tol=0.001):
    """
    Takes the eigenvalue with the largest real part
    
    returns a first guess of the
    maximal pseudoeigenvalue in the complex plane
    """
    evals = spl.eigvals(A)
    revals = np.real(evals)
    idxs = np.where(revals == np.amax(revals))[0]
    mevals = evals[idxs]
    iguesses = []
    for evl in mevals:
        guess = []
        a, b = np.real(evl), np.imag(evl)
        if a > 0:
            guess = [a, b]
        else:
            guess = [tol, b]
        iguesses.append(guess)
    return iguesses


def characterize_transient(A, tol=0.001, theta=0, ord=2):
    """
    returns the maximal eigenvalue (spectral abcissa),
    initial groth rate (numerical abcissa),
    the Kreiss constant (minimum bound of transient)
    and time of transient growth

    inputs:
    A: an MxM matrix
    tol: Used to find a first estimate of the pseudospectrum
    theta: normalizing factor found in Townley et al 2007, default 0
    ord: default 2, order of matrix norm

    returns: [Kreiss constant, numerical abcissa, duration of transient]
    
    >>> print(np.round(np.array(characterize_transient([[-1,500],[0,-2]]),dtype=np.complex64), 5))
    [ -1.     +0.j 248.5005 +0.j  85.78943+0.j   3.14984+0.j 500.     +0.j]
    
    """
    guesses = _first_estimate(A, tol)
    transient_properties = [1, 0, 0]
    for guess in guesses:
        tp = _transient_properties(guess, A, theta, ord)
        if tp[2] > transient_properties[2]:
            transient_properties = tp
        else:
            pass
    return transient_properties


def solve_ivp(M, t, x0, *args, **kwargs):
    """convenience wrapper for evolution due to matrix operator
    solves \dot{x}=M\cdot x

    Inputs:
    M: MxM matrix
    t: timepoints
    x0: initial state of length M

    returns:
    same as scipy.integrate.solve_ivp
    """
    def matrixMul(t, y, M):
        y = np.array(y)
        M = np.array(M)
        return M@y
    sol = spi.solve_ivp(matrixMul, (np.min(t), np.max(t)), x0, t_eval=t,
                        args=[M], **kwargs)
    return sol


if __name__ == '__main__':
    doctest.testmod()
    # print(solve_ivp([[-1, 2], [3, 4]], np.arange(0, 20), [2, 3], method="LSODA"))
    print(characterize_transient([[-1, 100], [0, -10]], theta=-.1),characterize_transient([[-1,500],[0,-2]]))
    # print(_max_pseudo_eval([3,1], [[1,2],[3,4]],method="svd"))
    # print(_epsilon_eval([1,3], [[1,2],[3,4]],method=-np.inf))
    # print(_kreiss_eval([1,2], [[1,2],[3,4]],0))
