import numpy as np
import scipy.integrate as spi
import scipy.linalg as spl
import matplotlib.pyplot as plt
from kreisspy import solve_ivp, characterize_transient


def plot(t, M, x0, *args, plot_all=True, matrix_characteristic=False, **kwargs):
    """
    Solves \dot{x} = M\cdot x
    Plots the magnitude ||x0|| with initial x0
    along with each component of x0

    Inputs:
    t: time array
    M: matrix
    x0: initial array
    plot_all: Bool plot all lines on graph. False plots only the total
    matrix_characteristic: Bool adds annotations to plot given by 
    characterize_transient
    """
    norm0 = spl.norm(x0)
    sol = solve_ivp(M, t, x0, *args, **kwargs)
    f, ax = plt.subplots()
    norm = spl.norm(sol.y,axis=0)
    if plot_all==True:
        ax.plot(t, sol.y.T/norm0)
    ax.plot(t,norm/norm0,color="blue")
    ax.set_xlabel("time")
    ax.set_ylabel(r'$x/||x||_0$')
    ax.set_title(r'$\dot{x}=M\cdot x$')
    ax.set_ylim((-.1,np.max(norm/norm0)*1.1))
    if matrix_characteristic==True:
        mc = characterize_transient(M)
       
        try:
            t_trunc = t[np.where(t<mc[3])]
            ev = np.max(spl.eigvals(M))
            ax.plot(t_trunc,np.exp(mc[1]*t_trunc),"--",color="orange")
        except RuntimeWarning:
            t_trunc = t[np.where(t<mc[3]/2)]
            ev = np.max(spl.eigvals(M))
            ax.plot(t_trunc,np.exp(mc[1]*t_trunc),"--",color="orange")
        ax.plot(t, np.exp(ev*t),"--",color="darkgreen")
        plt.axhline(y=mc[2],linestyle="dotted",color="black")
        if 3*mc[3]<t[-1]:
            plt.axvline(x=mc[3],linestyle="dotted",color="black")
        ax.set_xlim((-.1, np.min([3*mc[3],t[-1]])))
    return


if __name__ == '__main__':
    from kreisspy import characterize_transient
    M=[[-1,100],[0,-2]]
    plot(np.arange(0, 20,0.1), M, [0,1],  matrix_characteristic=True)
